<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_id')->nullable();
            $table->double('amount',8,2)->default(0.00);
            $table->enum('status',['pending','approved','failed'])->nullable();
            $table->integer('indigency_id')->nullable();
            $table->integer('business_permit_id')->nullable();
            $table->integer('business_clearance_id')->nullable();
            $table->integer('barangay_clearance_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
