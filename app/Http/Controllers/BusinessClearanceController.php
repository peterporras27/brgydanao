<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BusinessClearance;
use App\Mail\NotifyMail;
use Mail;
use Auth;

class BusinessClearanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $status = [
            'pending' => 'secondary',
            'approved' => 'success',
            'denied' => 'danger'
        ];

        if ($user->role == 'admin') {
            $datas = BusinessClearance::paginate(10);
        } else {
            $datas = BusinessClearance::where('user_id','=',$user->id)->paginate(10);
        }

        return view('business_clearance.index',compact('datas','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('business_clearance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        request()->validate([
            'owner_name' => 'required|string',
            'business_account_code' => 'required|string',
            'business_name' => 'required|string',
            'business_nature' => 'required|string',
            'business_location' => 'required|string'
        ]);

        $bc = new BusinessClearance;
        $bc->fill($request->all());
        $bc->start_date = date('Y-m-d');
        $bc->end_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 365 day'));
        $bc->user_id = $user->id;
        $bc->save();

        return redirect()->route('home')->with('success','Your request has been submitted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bc = BusinessClearance::find($id);

        if (!$bc) {
            return redirect()->route('business_clearance')->with('error','Request no longer exist.');
        }

        $bc->fill($request->all());
        $bc->save();

        $status = $request->input('status');

        if ( $status != 'pending') 
        {
            $name = $bc->user->first_name.' '.$bc->user->last_name;
            $message_one = 'We are here to inform you that your request for a Business Clearance has been '.$request->input('status').'.';
            $message_two = 'Please pick up your documents and prepare an amount of PHP 50.00 as payment for the service.';

            if ($status == 'denied') {
                $message_two = 'Please visit us at our office for farther clarifications.';
            }

            Mail::to($bc->user->email)->send(new NotifyMail($name,$message_one,$message_two));
        }

        return redirect()->route('business_clearance')->with('success','Request successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bc = BusinessClearance::find($id);

        if (!$bc) {
            return redirect()->route('business_clearance')->with('error','Request no longer exist.');
        }

        $bc->delete();
        return redirect()->route('business_clearance')->with('success','Request successfully removed.');
    }
}
