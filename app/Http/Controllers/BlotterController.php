<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blotter;
use App\Mail\NotifyMail;
use Mail;
use Auth;

class BlotterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $status = [
            'pending' => 'secondary',
            'approved' => 'success',
            'denied' => 'danger'
        ];

        if ($user->role == 'admin') {
            $datas = Blotter::paginate(10);
        } else {
            $datas = Blotter::where('user_id','=',$user->id)->paginate(10);
        }

        return view('blotter.index',compact('datas','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blotter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        
        $request->validate([
            'complainants' => ['required', 'string'],
            'respondents' => ['required', 'string'],
            'complaints' => ['required', 'string']
        ]);

        $ind = new Blotter;
        $ind->fill($request->all());
        $ind->date = date('Y-m-d');
        $ind->case_number = rand(1000,1000000000);
        $ind->user_id = $user->id;
        $ind->save();

        return redirect()->route('home')->with('success','Your blotter report has been submitted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ind = Blotter::find($id);

        if (!$ind) {
            return redirect()->route('blotter')->with('error','Request no longer exist.');
        }

        $ind->fill($request->all());
        $ind->save();

        $status = $request->input('status');

        if ( $status != 'pending') 
        {
            $name = $ind->user->first_name.' '.$ind->user->last_name;
            $message_one = 'We are here to inform you that your plee for a Blotter report has been '.$request->input('status').'.';
            $message_two = '';

            if ($status == 'denied') {
                $message_two = 'Please visit us at our office for farther clarifications.';
            }

            Mail::to($ind->user->email)->send(new NotifyMail($name,$message_one,$message_two));
        }

        return redirect()->route('blotter')->with('success','Request successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ind = Blotter::find($id);

        if (!$ind) {
            return redirect()->route('blotter')->with('error','Request no longer exist.');
        }

        $ind->delete();
        return redirect()->route('blotter')->with('success','Request successfully removed.');
    }
}
