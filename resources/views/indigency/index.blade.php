@include('inc.header')
@include('inc.navbar')

<div class="container mt-5">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header">
					Requested Reports
					@if(auth()->user()->role=='user')
					<form action="{{ route('indigency_store') }}" class="float-end" method="POST">
						@csrf
						<button type="submit" class="btn btn-sm rounded-pill btn-success float-end">Request New Certificate</button>	
					</form>
					@endif
				</div>
				<div class="card-body">
					<h5 class="card-title">Certificate of Indigency</h5>
					<p class="card-text">List of all certificate requested listed below.</p>
					<hr>
					@include('inc.errors')
					@if( $datas->count() )
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Fee</th>
									<th>Date Requested</th>
									<th>Valid Until</th>
									<th>Status</th>
									<th class="text-end">Options</th>
								</tr>
							</thead>
							<tbody>
							@foreach($datas as $data)
								<tr class="table-{{ $status[$data->status] }} align-middle">
									<td>{{ $data->user->first_name.' '.$data->user->last_name }}</td>
									<td>₱{{ $data->fee }}</td>
									<td>{{ date('F j, Y', strtotime($data->start_date)) }}</td>
									<td>{{ date('F j, Y', strtotime($data->end_date)) }} 
										@if( strtotime(date('d-m-Y')) > strtotime($data->end_date))
											<span class="badge bg-danger rounded-pill">Expired</span>
										@else 
											@if($data->status == 'approved')
												<span class="badge bg-success rounded-pill">Valid</span>
											@else
												<span class="badge bg-secondary rounded-pill">Pending</span>
											@endif
										@endif
									</td>
									<td>
										@if(auth()->user()->role=='admin')
											@if($data->status == 'approved')
												{{ ucfirst( $data->status ) }}
											@else
											<form action="{{ route('indigency_update',$data->id) }}" method="POST">
												@csrf
												@method('PUT')
												<div class="input-group">
													<select name="status" class="form-control form-control-sm" aria-label="Status" aria-describedby="button-addon2">
														<option value="pending"{{ $data->status == 'pending' ? ' selected':'' }}>Pending</option>
														<option value="approved"{{ $data->status == 'approved' ? ' selected':'' }}>Approved</option>
														<option value="denied"{{ $data->status == 'denied' ? ' selected':'' }}>Denied</option>
													</select>
													<button class="btn btn-outline-secondary btn-sm" type="submit" id="button-addon2">Update</button>
												</div>
											</form>
											@endif
										@else
											{{ ucfirst( $data->status ) }}
										@endif
									</td>
									<td>
										@if(auth()->user()->role=='admin')
											<a href="#" data-bs-toggle="modal" data-bs-target="#ind-{{$data->id}}" class="badge bg-danger float-end">Delete</a>
											<!-- Modal -->
											<div class="modal fade" id="ind-{{$data->id}}" tabindex="-1" aria-labelledby="ind-{{$data->id}}-Label" aria-hidden="true">
												<div class="modal-dialog">
													<div class="modal-content">
														<form action="{{ route('indigency_destroy',$data->id) }}" method="POST">
															@csrf
															@method('DELETE')
															<div class="modal-header">
																<h5 class="modal-title" id="ind-{{$data->id}}-Label">Certificate of Indigency</h5>
																<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
															</div>
															<div class="modal-body">
																Are you sure you wish to delete this request?
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
																<button type="submit" class="btn btn-danger">Delete</button>
															</div>
														</form>
													</div>
												</div>
											</div>
										@endif
										@if($data->status == 'approved')
											<a href="{{ route('indigency_show',$data->id) }}" target="_blank" class="badge bg-success float-end" style="margin-right: 15px;">Print Certificate</a>
										@endif
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						{{ $datas->links() }}
					@else
						<div class="alert alert-primary" role="alert">
							There are no certificates to display at the moment.
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

@include('inc.footer')