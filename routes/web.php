<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\AdminController;

use App\Http\Controllers\IndigencyController;
use App\Http\Controllers\ResidencyController;
use App\Http\Controllers\BlotterController;
use App\Http\Controllers\BarangayClearanceController;
use App\Http\Controllers\BusinessClearanceController;

use App\Http\Controllers\GoogleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'index'])->name('home');
Route::get('dashboard', [AdminController::class, 'index'])->name('dashboard');
Route::get('settings', [AdminController::class, 'settings'])->name('settings');
Route::post('settings', [AdminController::class, 'settings_update'])->name('settings_update');

Route::get('certificate-of-indigency', [IndigencyController::class, 'index'])->name('indigency');
Route::post('certificate-of-indigency', [IndigencyController::class, 'store'])->name('indigency_store');
Route::get('certificate-of-indigency/{id}', [IndigencyController::class, 'show'])->name('indigency_show');
Route::get('certificate-of-indigency/{id}/edit', [IndigencyController::class, 'edit'])->name('indigency_edit');
Route::put('certificate-of-indigency/{id}', [IndigencyController::class, 'update'])->name('indigency_update');
Route::delete('certificate-of-indigency/{id}/delete', [IndigencyController::class, 'destroy'])->name('indigency_destroy');

Route::get('certificate-of-residency', [ResidencyController::class, 'index'])->name('residency');
Route::post('certificate-of-residency', [ResidencyController::class, 'store'])->name('residency_store');
Route::get('certificate-of-residency/{id}', [ResidencyController::class, 'show'])->name('residency_show');
Route::get('certificate-of-residency/{id}/edit', [ResidencyController::class, 'edit'])->name('residency_edit');
Route::put('certificate-of-residency/{id}', [ResidencyController::class, 'update'])->name('residency_update');
Route::delete('certificate-of-residency/{id}/delete', [ResidencyController::class, 'destroy'])->name('residency_destroy');

Route::get('blotter-report', [BlotterController::class, 'index'])->name('blotter');
Route::get('blotter-report/create', [BlotterController::class, 'create'])->name('blotter_create');
Route::post('blotter-report', [BlotterController::class, 'store'])->name('blotter_store');
Route::get('blotter-report/{id}', [BlotterController::class, 'show'])->name('blotter_show');
Route::get('blotter-report/{id}/edit', [BlotterController::class, 'edit'])->name('blotter_edit');
Route::put('blotter-report/{id}', [BlotterController::class, 'update'])->name('blotter_update');
Route::delete('blotter-report/{id}/delete', [BlotterController::class, 'destroy'])->name('blotter_destroy');

Route::get('barangay-clearance', [BarangayClearanceController::class, 'index'])->name('brgy_clearance');
Route::get('barangay-clearance/create', [BarangayClearanceController::class, 'create'])->name('brgy_clearance_create');
Route::post('barangay-clearance', [BarangayClearanceController::class, 'store'])->name('brgy_clearance_store');
Route::get('barangay-clearance/{id}', [BarangayClearanceController::class, 'show'])->name('brgy_clearance_show');
Route::get('barangay-clearance/{id}/edit', [BarangayClearanceController::class, 'edit'])->name('brgy_clearance_edit');
Route::put('barangay-clearance/{id}', [BarangayClearanceController::class, 'update'])->name('brgy_clearance_update');
Route::delete('barangay-clearance/{id}/delete', [BarangayClearanceController::class, 'destroy'])->name('brgy_clearance_destroy');

Route::get('business-clearance', [BusinessClearanceController::class, 'index'])->name('business_clearance');
Route::get('business-clearance/create', [BusinessClearanceController::class, 'create'])->name('business_clearance_create');
Route::post('business-clearance', [BusinessClearanceController::class, 'store'])->name('business_clearance_store');
Route::get('business-clearance/{id}', [BusinessClearanceController::class, 'show'])->name('business_clearance_show');
Route::get('business-clearance/{id}/edit', [BusinessClearanceController::class, 'edit'])->name('business_clearance_edit');
Route::put('business-clearance/{id}', [BusinessClearanceController::class, 'update'])->name('business_clearance_update');
Route::delete('business-clearance/{id}/delete', [BusinessClearanceController::class, 'destroy'])->name('business_clearance_destroy');

Route::controller(GoogleController::class)->group(function(){
    Route::get('auth/google', 'redirectToGoogle')->name('auth.google');
    Route::get('auth/google/callback', 'handleGoogleCallback');
});

require __DIR__.'/auth.php';

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return redirect('/');
    })->name('dashboard');
});
