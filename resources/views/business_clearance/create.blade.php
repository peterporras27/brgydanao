@include('inc.header')
@include('inc.navbar')

<div class="container mt-5">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header">
					Request Business Clearance
				</div>
				<div class="card-body">
					@include('inc.errors')
					
					<form action="{{ route('business_clearance_store') }}" method="POST">
						@csrf
						
						<div class="mb-3">
							<label for="owner_name" class="form-label">Owner Name:</label>
							<input name="owner_name" type="text" class="form-control" id="owner_name" value="{{ old('owner_name') }}">
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="business_name" class="form-label">Business Name:</label>
									<input name="business_name" type="text" class="form-control" id="business_name" value="{{ old('business_name') }}">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="business_nature" class="form-label">Nature of Business</label>
									<input name="business_nature" type="text" class="form-control" id="business_nature" value="{{ old('business_nature') }}">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="business_location" class="form-label">Business Address:</label>
									<input name="business_location" type="text" class="form-control" id="business_location" value="{{ old('business_location') }}">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="business_account_code" class="form-label">Business Account Code</label>
									<input name="business_account_code" type="text" class="form-control" id="business_account_code" value="{{ old('business_account_code') }}">
								</div>
							</div>
						</div>
						
						<button type="submit" class="btn btn-primary">
							Submit Request
						</button>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>

@include('inc.footer')