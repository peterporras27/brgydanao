<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BarangayClearance;
use App\Mail\NotifyMail;
use Mail;
use Auth;


class BarangayClearanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $status = [
            'pending' => 'secondary',
            'approved' => 'success',
            'denied' => 'danger'
        ];

        if ($user->role == 'admin') {
            $datas = BarangayClearance::paginate(10);
        } else {
            $datas = BarangayClearance::where('user_id','=',$user->id)->paginate(10);
        }

        return view('brgy_clearance.index',compact('datas','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $hasValid = BarangayClearance::where([
            ['status','=','pending'],
            ['user_id','=',$user->id]
        ])->first();

        if ($hasValid) {
            return redirect()->route('brgy_clearance')->with('error','You have an existing request, please wait for admin approval.');
        }

        $ind = new BarangayClearance;
        $ind->start_date = date('Y-m-d');
        $ind->end_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 365 day'));
        $ind->user_id = $user->id;
        $ind->save();

        return redirect()->route('home')->with('success','Your request has been submitted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ind = BarangayClearance::find($id);
        $strtime = strtotime($ind->start_date);
        $day = date('jS', $strtime);
        $month = date('F', $strtime);
        $year = date('Y', $strtime);

        return view('residency.print',compact('ind','day','month','year'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ind = BarangayClearance::find($id);

        if (!$ind) {
            return redirect()->route('brgy_clearance')->with('error','Request no longer exist.');
        }

        $ind->fill($request->all());
        $ind->save();

        $status = $request->input('status');

        if ( $status != 'pending') 
        {
            $name = $ind->user->first_name.' '.$ind->user->last_name;
            $message_one = 'We are here to inform you that your request for Barangay Clearance has been '.$request->input('status').'.';
            $message_two = 'Please pick up your documents and prepare an amount of PHP 50.00 as payment for the service.';

            if ($status == 'denied') {
                $message_two = 'Please visit us at our office for farther clarifications.';
            }

            Mail::to($ind->user->email)->send(new NotifyMail($name,$message_one,$message_two));
        }

        return redirect()->route('brgy_clearance')->with('success','Request successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ind = BarangayClearance::find($id);

        if (!$ind) {
            return redirect()->route('brgy_clearance')->with('error','Request no longer exist.');
        }

        $ind->delete();
        return redirect()->route('brgy_clearance')->with('success','Request successfully removed.');
    }
}
