<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'transaction_id',
        'amount',
        'status',
        'indigency_id',
        'business_permit_id',
        'business_clearance_id',
        'barangay_clearance_id',
        'user_id'
    ];
}
