<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $admin = User::create([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'middle_name' => '',
            'role' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('GABIANOO12345')
        ]);

        $newadmin = User::create([
            'first_name' => 'Brgy. Danao',
            'last_name' => 'Secretary',
            'middle_name' => '',
            'role' => 'admin',
            'email' => 'admin@brgydanao.tk',
            'password' => bcrypt('secret')
        ]);
    }
}
