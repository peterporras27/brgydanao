@include('inc.header')
@include('inc.navbar')

<div class="container mt-5">
	<div class="row">
		<div class="col-sm-6">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Certificate of Indegency</h5>
				</div>
				<div class="card-body">
					<p class="card-text">Send us a request for your certificate of indigency.</p>
				</div>
				<div class="card-footer">
					@if(!auth()->check())
					
						<a href="{{ route('login') }}" class="btn btn-success rounded-pill">Request Certificate</a>
					
					@else

						@if(auth()->user()->role=='user')
							<form action="{{ route('indigency_store') }}" method="POST">
								@csrf
								<button type="submit" class="btn btn-success rounded-pill">Request Certificate</button>	
								<a href="{{ route('indigency') }}" class="btn btn-secondary rounded-pill float-end">View Certificates</a>
							</form>
						@else
							<a href="{{ route('indigency') }}" class="btn btn-dark rounded-pill">Manage Requests</a>
						@endif
						
					@endif
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Certificate of Residency</h5>
				</div>
				<div class="card-body">
					<p class="card-text">Send us a request for your certificate of residency.</p>
				</div>
				<div class="card-footer">
					@if(!auth()->check())

						<a href="{{ route('login') }}" class="btn btn-success rounded-pill">Request Certificate</a>
					
					@else

						@if(auth()->user()->role=='user')
							<form action="{{ route('residency_store') }}" method="POST">
								@csrf
								<button type="submit" class="btn btn-success rounded-pill">Request Certificate</button>	
								<a href="{{ route('residency') }}" class="btn btn-secondary rounded-pill float-end">View Certificates</a>
							</form>
						@else
							<a href="{{ route('residency') }}" class="btn btn-dark rounded-pill">Manage Requests</a>
						@endif
					
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-4">
		<div class="col-sm-6">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Blotter Report</h5>
				</div>
				<div class="card-body">
					<p class="card-text">Submit a blotter report to our barangay.</p>
				</div>
				<div class="card-footer">
					@if(!auth()->check())
						<a href="{{ route('blotter_create') }}" class="btn btn-success rounded-pill">Submit Blotter Report</a>
					@else
						@if(auth()->user()->role=='user')
							<a href="{{ route('blotter_create') }}" class="btn btn-success rounded-pill">Submit Blotter Report</a>
							<a href="{{ route('blotter') }}" class="btn btn-secondary rounded-pill float-end">View Reports</a>
						@else
							<a href="{{ route('blotter') }}" class="btn btn-dark rounded-pill">Manage Requests</a>
						@endif
					@endif
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Barangay Clearance</h5>
				</div>
				<div class="card-body">
					<p class="card-text">Request for your barnagay clearance.</p>
				</div>
				<div class="card-footer">
					@if(!auth()->check())
						<a href="{{ route('login') }}" class="btn btn-success rounded-pill">Request Clearance</a>
					@else
						@if(auth()->user()->role=='user')
							<form action="{{ route('brgy_clearance_store') }}" method="POST">
								@csrf
								<button type="submit" class="btn btn-success rounded-pill">Request Clearance</button>	
								<a href="{{ route('brgy_clearance') }}" class="btn btn-secondary float-end rounded-pill">View Clearances</a>
							</form>
						@else
							<a href="{{ route('brgy_clearance') }}" class="btn btn-dark rounded-pill">Manage Clearances</a>
						@endif
					
					@endif
				</div>
			</div>
		</div>
	</div>

	<div class="row mt-4 justify-content-center">
		<div class="col-sm-6">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Business Clearance</h5>
				</div>
				<div class="card-body">
					<p class="card-text">Request of your business clearance.</p>
				</div>
				<div class="card-footer">
					@if(!auth()->check())
						<a href="{{ route('login') }}" class="btn btn-success rounded-pill">Request Business Clearance</a>
					@else
						@if(auth()->user()->role=='user')
							<a href="{{ route('business_clearance_create') }}" class="btn btn-success rounded-pill">Request Business Clearance</a>
							<a href="{{ route('business_clearance') }}" class="btn btn-secondary rounded-pill float-end">Manage Clearances</a>
						@else
							<a href="{{ route('business_clearance') }}" class="btn btn-dark rounded-pill">Manage Clearances</a>
						@endif
					
					@endif
				</div>
			</div>
		</div>
	</div>
	
</div>

@include('inc.footer')