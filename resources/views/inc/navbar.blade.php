<nav class="navbar navbar-expand-lg navbar-light">
	<div class="container">
		<a class="navbar-brand" href="/">Brgy. Danao Janiuay Iloilo</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
				@if (Route::has('login'))
                    @auth
	                    @if(Auth::user()->role=='admin')
	                    <!-- <li class="nav-item">
	                        <a href="#" class="nav-link">Dashboard</a>
	                    </li> -->
	                    @endif
                    @endauth
	            @endif
			</ul>
			<span class="navbar-text">
				<ul class="navbar-nav">
					@if (Route::has('login'))
						@auth
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
									@if(Auth::user()->first_name)
										{{ Auth::user()->first_name.' '.Auth::user()->last_name }}
									@else
										{{ Auth::user()->name }}
									@endif
								</a>
								<ul class="dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
									<li><a class="dropdown-item" href="{{ route('settings') }}">Settings</a></li>
									<li>
										<form method="POST" action="{{ route('logout') }}">
						                    @csrf
						                    <button class="dropdown-item" href="route('logout')" onclick="event.preventDefault();
						                                        this.closest('form').submit();">Log Out</button>
						                </form>
									</li>
								</ul>
							</li>

						@else
							<li class="nav-item">
								<a href="{{ route('login') }}" class="nav-link">Log in</a>
							</li>
	                        @if (Route::has('register'))
	                        <li class="nav-item">
	                            <a href="{{ route('register') }}" class="nav-link">Register</a>
	                        </li>
	                        @endif
                        @endauth
                    @endif
				</ul>
			</span>
		</div>
	</div>
</nav>