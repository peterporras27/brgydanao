@include('inc.header')
@include('inc.navbar')

<div class="container mt-5">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header">
					Create Blotter Report
				</div>
				<div class="card-body">
					@include('inc.errors')
					
					<form action="{{ route('blotter_store') }}" method="POST">
						@csrf
						<div class="row">
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="complainants" class="form-label">Complainants</label>
									<input name="complainants" type="text" class="form-control" id="complainants" value="{{ old('complainants') }}">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="respondents" class="form-label">Respondents</label>
									<input name="respondents" type="text" class="form-control" id="respondents" value="{{ old('respondents') }}">
								</div>
							</div>
						</div>

						<div class="mb-3">
							<label for="complaints" class="form-label">
								I / We hereby complain against the above - named respondents for violating my:
							</label>
							<textarea name="complaints" class="form-control" id="complaints" rows="3">{{ old('complaints') }}</textarea>
						</div>
						
						<button type="submit" class="btn btn-primary">
							Submit Report
						</button>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>

@include('inc.footer')