<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('styles.css') }}">
<title>Brgy. Danao</title>
</head>
<body>
	@include('inc.navbar')

	{{ $slot }}

	<script src="{{ asset('jquery-3.6.0.min.js') }}"></script>
	<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('scripts.js') }}"></script>
</body>
</html>