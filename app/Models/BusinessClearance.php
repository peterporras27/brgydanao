<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessClearance extends Model
{
    use HasFactory;

    protected $fillable = [
        'owner_name',
        'business_account_code',
        'business_name',
        'business_nature',
        'business_location',
        'start_date',
        'end_date',
        'status',
        'fee',
        'user_id',
    ];

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
