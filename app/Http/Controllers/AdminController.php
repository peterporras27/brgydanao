<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return redirect('/');
    }

    public function settings()
    {
        $user = Auth::user();
        return view('pages.settings', compact('user'));
    }

    public function settings_update(Request $request)
    {
        $user = Auth::user();
        $datas = $request->all();
        $data = [
            'first_name' => 'required|string',
            'middle_name' => 'required|string',
            'last_name' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'gender' => 'required|string',
            'birthday' => 'required|date',
            'vacinated' => 'required|boolean'
        ];

        if ( $request->input('email') != $user->email ) {
            $data['email'] = 'required|string|email|max:255|unique:users';
        } else {
            unset($datas['email']);
        }

        if ( !empty( $request->input('password') ) ) {
            $data['password'] = 'required|confirmed';
        } else {
            unset($datas['password']);
        }

        request()->validate($data);

        $user->fill($datas);
        $user->save();

        return redirect()->route('settings')->with('success','Profile successfully updated!');
    }
}
