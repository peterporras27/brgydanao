@include('inc.header')
@include('inc.navbar')

<div class="container mt-5">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header">
					Profile Information
				</div>
				<div class="card-body">

					@include('inc.errors')

					<form action="{{ route('settings_update') }}" method="POST">
						@csrf
						<div class="row">
							<div class="col-sm-4">
								<div class="mt-3">
									<label for="first_name" class="form-label">First Name</label>
									<input type="text" name="first_name" id="first_name" value="{{ $user->first_name }}" class="form-control">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="mt-3">
					            	<label for="middle_name" class="form-label">Middle Name</label>
					            	<input type="text" name="middle_name" id="middle_name" value="{{ $user->middle_name }}" class="form-control">
					            </div>
							</div>
							<div class="col-sm-4">
					            <div class="mt-3">
					            	<label for="last_name" class="form-label">Last Name</label>
					            	<input type="text" name="last_name" id="last_name" value="{{ $user->last_name }}" class="form-control">
					            </div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="mt-3">
					            	<label for="email" class="form-label">Email</label>
					            	<input type="email" name="email" id="email" value="{{ $user->email }}" class="form-control">
					            </div>
							</div>
							<div class="col-sm-6">
								<div class="mt-3">
					            	<label for="phone" class="form-label">Phone</label>
					            	<input type="number" name="phone" id="phone" value="{{ $user->phone }}" class="form-control">
					            </div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="mt-3">
					            	<label for="address" class="form-label">Address</label>
					            	<input type="text" name="address" id="address" value="{{ $user->address }}" class="form-control">
					            </div>
							</div>
							<div class="col-sm-6">
								 <div class="mt-3">
					            	<label for="gender" class="form-label">Gender</label>
					            	<select type="text" name="gender" id="gender" class="form-control">
					            		<option value="Male"{{ $user->gender == 'Male' ? ' selected':''}}>Male</option>
					            		<option value="Female"{{ $user->gender == 'Female' ? ' selected':''}}>Female</option>
					            	</select>
					            </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="mt-3">
					            	<label for="birthday" class="form-label">Birthday</label>
					            	<input type="date" name="birthday" id="birthday" value="{{ $user->birthday }}" class="form-control">
					            </div>
							</div>
							<div class="col-sm-6">
								<div class="mt-5">
					            	<label for="vacinated" class="form-label">
					            		<input type="checkbox" name="vacinated" value="1" id="vacinated" {{ $user->vacinated ? ' checked':''}}> Are you fully vacinated?
					            	</label>
					            </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="mt-3">
					            	<label for="password" class="form-label">Password</label>
					            	<input type="password" name="password" id="password" class="form-control">
					            </div>
							</div>
							<div class="col-sm-6">
								<div class="mt-3">
					            	<label for="password_confirmation" class="form-label">Confirm Password</label>
					            	<input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
					            </div>
							</div>
						</div>
						<div class="mt-3">
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
		            </form>
				</div>
			</div>
		</div>
	</div>
</div>

@include('inc.footer')