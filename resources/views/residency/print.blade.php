@include('inc.header')

<div class="outer-border">
<div class="outer-line-border">
<div class="center-border">
<div class="inner-line-border">
<div class="inner-border">

	<div class="text-center sans heading">
		<div>Republic of The Philippines</div>
		<div>Province of Iloilo</div>
		<div>Municipality of Janiuay</div>
		<div>Barangay Danao</div>
	</div>
	<h1 class="cursive text-center">Certificate of Residency</h1>
	<br>

	<div class="row justify-content-center">
		<div class="col-10 sans">
			<p><b>TO WHOM IT MAY CONCERN:</b></p>
			<p><span class="space"></span>This is to certify that <b>{{ $ind->user->first_name . ' ' . $ind->user->midle_name .' '.$ind->user->last_name  }}</b>, <b>{{ $ind->user->age() }} years old</b> is a bonafade resident of Brgy. Danao Janiuay, Iloilo City.</p>
			<p><span class="space"></span>This certification is being issued for whatever legal purpose it may serve.</p>
			<p><span class="space"></span>ISSUED this <b>{{ $day }}</b> day of <b>{{ $month }} {{ $year }}</b> at Brgy. Danao Janiuay, Iloilo City.</p>
		</div>
	</div>

	<div class="row justify-content-center align-content-between">
		<div class="col-10">
			<div class="row ">
				<div class="col">
					<div class="auth">
						<b>For Authenticity, Validity and Record Purpose:</b><br>
						Control Number: 00{{$ind->id}}<br>
						Series of {{ $year }}<br>
						<div class="text-center">
							<img src="{{asset('img/duerme.png')}}" alt="" class="img-fluid">
							<br><br><br>
							<strong><u>JANICE G. DUERME</u></strong><br>
							Brangay Treasurer
						</div>
					</div>
				</div>
				<div class="col">
					<div class="capt float-end">
						<div class="text-center">
							<img src="{{asset('img/martinez.png')}}" alt="" class="img-fluid">
							<br><br>
							<strong><u>REY V. MARTINEZ</u></strong><br>
							Punong Barangay
						</div>
					</div>
				</div>
			</div>


			
		</div>
	</div>
	
</div>
</div>
</div>
</div>
</div>
<script>window.print();</script>
@include('inc.footer')