@if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <p>{{ __('Whoops! Something went wrong.') }}</p>
        <ul class="mt-3 list-disc list-inside text-sm text-red-600">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('success'))
    <div class="alert alert-success" role="alert">
        <b>Success!</b> {{ session('success') }}
    </div>
@endif


@if (session('error'))
    <div class="alert alert-danger" role="alert">
        <b>Error!</b> {{ session('error') }}
    </div>
@endif
