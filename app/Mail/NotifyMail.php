<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $message_one;
    public $message_two;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$message_one,$message_two)
    {
        $this->name = $name;
        $this->message_one = $message_one;
        $this->message_two = $message_two;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail')->with([
            'name' => $this->name,
            'message_one' => $this->message_one,
            'message_two' => $this->message_one
        ]);
    }
}
