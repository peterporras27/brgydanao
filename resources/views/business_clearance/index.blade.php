@include('inc.header')
@include('inc.navbar')

<div class="container mt-5">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header">
					Requested Reports
					<a href="{{ route('business_clearance_create') }}" class="btn btn-sm rounded-pill btn-success float-end">Request Business Clearance</a>	
				</div>
				<div class="card-body">
					<h5 class="card-title">Business Clearances</h5>
					<p class="card-text">List of all business clearance request listed below.</p>
					<hr>
					@include('inc.errors')
					@if( $datas->count() )
						<table class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th>Owner Name</th>
									<th>Business Name</th>
									<th>Business Address</th>
									<th>Validity</th>
									<th>Status</th>
									@if(auth()->user()->role=='admin')
									<th class="text-end">Options</th>
									@endif
								</tr>
							</thead>
							<tbody>
							@foreach($datas as $data)
								<tr class="table-{{ $status[$data->status] }}">
									<td>{{ $data->owner_name }}</td>
									<td>{{ $data->business_name }}</td>
									<td>{{ $data->business_location }}</td>
									<td>{{ date('F j, Y', strtotime($data->start_date)) }} - {{ date('F j, Y', strtotime($data->end_date)) }}</td>
									<td>
										@if(auth()->user()->role=='admin')
											@if($data->status == 'approved')
												{{ ucfirst( $data->status ) }}
											@else
											<form action="{{ route('business_clearance_update',$data->id) }}" method="POST">
												@csrf
												@method('PUT')
												<div class="input-group">
													<select name="status" class="form-control form-control-sm" aria-label="Status" aria-describedby="button-addon2">
														<option value="pending"{{ $data->status == 'pending' ? ' selected':'' }}>Pending</option>
														<option value="approved"{{ $data->status == 'approved' ? ' selected':'' }}>Approved</option>
														<option value="denied"{{ $data->status == 'denied' ? ' selected':'' }}>Denied</option>
													</select>
													<button class="btn btn-outline-secondary btn-sm" type="submit" id="button-addon2">Update</button>
												</div>
											</form>
											@endif
										@else
											{{ ucfirst( $data->status ) }}
										@endif
									</td>
									@if(auth()->user()->role=='admin')
									<td>
										<a href="#" data-bs-toggle="modal" data-bs-target="#ind-{{$data->id}}" class="badge bg-danger float-end">Delete</a>
										<!-- Modal -->
										<div class="modal fade" id="ind-{{$data->id}}" tabindex="-1" aria-labelledby="ind-{{$data->id}}-Label" aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">
													<form action="{{ route('business_clearance_destroy',$data->id) }}" method="POST">
														@csrf
														@method('DELETE')
														<div class="modal-header">
															<h5 class="modal-title" id="ind-{{$data->id}}-Label">Certificate of Indigency</h5>
															<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
														</div>
														<div class="modal-body">
															Are you sure you wish to delete this request?
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
															<button type="submit" class="btn btn-danger">Delete</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</td>
									@endif
								</tr>
							@endforeach
							</tbody>
						</table>
						{{ $datas->links() }}
					@else
						<div class="alert alert-primary" role="alert">
							There are no certificates to display at the moment.
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

@include('inc.footer')