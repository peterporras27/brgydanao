<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blotter extends Model
{
    use HasFactory;

    protected $fillable = [
        'complainants',
        'respondents',
        'complaints',
        'case_number',
        'date',
        'status',
        'user_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
