### INSTALATION REQUIREMENTS

* Download XAMPP with PHP version 8.0 https://www.apachefriends.org/download.html
* Composer Version 2 Download and install Composer https://getcomposer.org/download/

### PROJECT SETUP

Open Command Prompt or any CLI and cd to the project folder and run these commands:
```
cp .env.example .env
composer update
php artisan key:generate
```

Open your `.env` file using code editor and Update your database details
```
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
Also update your SMTP account for sending emails.
```
MAIL_MAILER=smtp
MAIL_HOST=
MAIL_PORT=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=null
MAIL_FROM_NAME="${APP_NAME}"
MAIL_FROM_ADDRESS=
```

Migrate database by running this command once:
```
php artisan migrate --seed
```

Run application
```
php artisan serve
```

Open your web browser and go to [this link](http://localhost:8000)

Done!